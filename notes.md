# Midterm 1
-----

### Sumerian Period

#### Votive Statuettes (about 3000 BCE)
* Tell Asmar, Iraq
* c. 2900 BCE
* Stand-ins for prayers and wishes

#### Nana (Moon) Ziggurat (about 2000BCE)
* Ur, Middle East 
* c. 2100 BCE
* Meant to be climbed, communicate with the gods

-----

### Egyptian Period

#### Great Pyramids  (about 2500BCE)
* Giza, Egypt
* c. 2500 BCE
* Tombs for kings
* Not meant to be climbed, oriented with the sun rise

-----

### Greek

#### Krater (about 800BCE)
* Dipylon Cemetery, Greece
* c. 750 BCE

#### Kiritios Boy (about 500BCE)
* c. 470 BCE
* Gazing into distance, somewhat symmetrical 

#### Charioteer at Delphi (about 500BCE)
* Delphi, Greece
* c. 470 BCE
* Hubris

#### Spear Bearer (Doryphorus) (about 400BCE)
* By Polykleitos
* c. 440 BCE

#### The Parthenon (about 400BCE)
* By Iktinos and Kallikrates
* Athens, Greece
* c. 447 - 432 BCE

#### Stele of Hegeso
* c. 400 BCE

#### Aphrodite of Cnidos (about 400BCE)
* By Praxiteles
* c. 350 BCE

#### Gallic Chief killing his wife and himself (about 200BCE)
* By Hellenistic
* c. 220 BCE
* Action packed, very vivid

-----

### Buddhist

#### The Great Stupa at Sanchi
* Sanchi, India
* c. 300 BCE

#### Seated Buddha from Mathura
* c. 100 CE to c. 200 CE

#### Standing Buddha from Gandhara
* c. 200 CE to c. 300 CE

----- 

### Roman 

#### Colosseum
* Rome, Italy
* 80 CE

#### Pantheon
* Rome, Italy
* 125 CE

#### Augustus from Primaporta
* c. 20 BCE

#### Ara Pacis (Altar of Augustan Peace)
* c. 13 BCE to c. 9 BCE

#### Arch of Titus
* Rome, Italy
* after 81 CE

-----

### Early Christian

#### The Good Shepherd, Catacomb of Callistus
* Rome, Italy
* c. 200 CE

#### Sarcophagus of Junius Bassus
* c 359 CE

#### Luke
* Abba Garima Gospels III
* c. 370CE to c. 590 CE

#### Hagia Sophia
* By Anthemius of Tralles and Isidorus of Miletus
* Constantinople
* c. 532 CE to c. 537 CE

-----

### Byzantine

#### Mosaic of Good Shepherd at Mausoleum of Galla Placidia
* Ravenna, Italy
* c. 425 CE

#### Mosaic of Justinian, San Vitale
* Ravenna, Italy
* c. 546 CE to c. 548 CE

-----

### Romanesque

#### Reliquary of St. Foy
* c. 1000 CE

#### Church of St. Foy
* Conques, France

-----

### Terms
__frieze__ - Horizontal strip just below ceiling (between pediment and pillars)

__pediment__  - Triangular upper part of building
